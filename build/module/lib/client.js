import jwt_decode from 'jwt-decode';
import debug from 'debug';
const log = debug('api:Client');
export class Client {
    constructor(clientId, apiBase) {
        this.request = async (query, variables) => {
            const headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('X-APP-ID', this._clientId);
            if (this._token !== '') {
                headers.append('Authorization', `Bearer ${this._token}`);
                //check for expired token
                //throw new Error(response.statusText)
            }
            const request = new Request(this._apiBase, {
                method: "POST",
                headers: headers,
                body: JSON.stringify({
                    query,
                    variables,
                }),
            });
            const response = await fetch(request);
            let body;
            try {
                body = await response.json();
            }
            catch (e) {
                log('Cant parse body', e);
            }
            if (!response.ok) {
                throw new Error(response.statusText);
            }
            return {
                response,
                body,
            };
        };
        this._token = '';
        this._clientId = clientId;
        this._apiBase = apiBase;
    }
    setToken(token) {
        this._token = token;
    }
    decodeToken(token) {
        if (token === '') {
            return {
                error: 'invalid-token'
            };
        }
        else {
            return jwt_decode(token);
        }
    }
    checkExpired(token) {
        let isExpired = true;
        const decoded = this.decodeToken(token);
        if (decoded.error) {
            log(decoded.error);
        }
        else {
            let now = new Date();
            let expires = new Date(decoded.exp * 1000);
            if (now > expires) {
                log('Expired Token');
            }
            else {
                isExpired = false;
            }
        }
        return isExpired;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2xpYi9jbGllbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsT0FBTyxVQUFVLE1BQU0sWUFBWSxDQUFBO0FBQ25DLE9BQU8sS0FBSyxNQUFNLE9BQU8sQ0FBQTtBQUV6QixNQUFNLEdBQUcsR0FBWSxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUE7QUFFeEMsTUFBTSxPQUFPLE1BQU07SUFNZixZQUFhLFFBQWUsRUFBRSxPQUFlO1FBdUM3QyxZQUFPLEdBQUcsS0FBSyxFQUFFLEtBQWEsRUFBRSxTQUFjLEVBQUUsRUFBRTtZQUU5QyxNQUFNLE9BQU8sR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFBO1lBQzdCLE9BQU8sQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLGtCQUFrQixDQUFDLENBQUE7WUFDbEQsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFBO1lBQzFDLElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxFQUFFLEVBQUU7Z0JBQ3BCLE9BQU8sQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFFLFVBQVUsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUE7Z0JBQ3hELHlCQUF5QjtnQkFDekIsc0NBQXNDO2FBQ3pDO1lBRUQsTUFBTSxPQUFPLEdBQUcsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQXVCLEVBQUU7Z0JBQ3RELE1BQU0sRUFBRSxNQUFNO2dCQUNkLE9BQU8sRUFBRSxPQUFPO2dCQUNoQixJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQztvQkFDakIsS0FBSztvQkFDTCxTQUFTO2lCQUNaLENBQUM7YUFDTCxDQUFDLENBQUE7WUFFRixNQUFNLFFBQVEsR0FBRyxNQUFNLEtBQUssQ0FBRSxPQUFPLENBQUUsQ0FBQTtZQUN2QyxJQUFJLElBQUksQ0FBQTtZQUVSLElBQUk7Z0JBQ0EsSUFBSSxHQUFHLE1BQU0sUUFBUSxDQUFDLElBQUksRUFBRSxDQUFBO2FBQy9CO1lBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ1IsR0FBRyxDQUFDLGlCQUFpQixFQUFFLENBQUMsQ0FBQyxDQUFBO2FBQzVCO1lBRUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLEVBQUU7Z0JBQ2QsTUFBTSxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDeEM7WUFFRCxPQUFPO2dCQUNILFFBQVE7Z0JBQ1IsSUFBSTthQUNQLENBQUE7UUFDTCxDQUFDLENBQUE7UUEzRUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUE7UUFDaEIsSUFBSSxDQUFDLFNBQVMsR0FBSSxRQUFRLENBQUE7UUFDMUIsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUE7SUFDM0IsQ0FBQztJQUVELFFBQVEsQ0FBRSxLQUFhO1FBQ25CLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFBO0lBQ3ZCLENBQUM7SUFFRCxXQUFXLENBQUUsS0FBYTtRQUN0QixJQUFHLEtBQUssS0FBSyxFQUFFLEVBQUU7WUFDYixPQUFPO2dCQUNILEtBQUssRUFBRSxlQUFlO2FBQ3pCLENBQUE7U0FDSjthQUFNO1lBQ0gsT0FBTyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUE7U0FDM0I7SUFDTCxDQUFDO0lBRUQsWUFBWSxDQUFDLEtBQWE7UUFFdEIsSUFBSSxTQUFTLEdBQVksSUFBSSxDQUFBO1FBQ25DLE1BQU0sT0FBTyxHQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUE7UUFDM0MsSUFBRyxPQUFPLENBQUMsS0FBSyxFQUFFO1lBQ2pCLEdBQUcsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUE7U0FDbEI7YUFBTTtZQUNOLElBQUksR0FBRyxHQUFTLElBQUksSUFBSSxFQUFFLENBQUE7WUFDMUIsSUFBSSxPQUFPLEdBQVMsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsQ0FBQTtZQUNoRCxJQUFHLEdBQUcsR0FBRyxPQUFPLEVBQUM7Z0JBQ2hCLEdBQUcsQ0FBQyxlQUFlLENBQUMsQ0FBQTthQUNwQjtpQkFBTTtnQkFDTixTQUFTLEdBQUcsS0FBSyxDQUFBO2FBQ2pCO1NBQ0s7UUFFUCxPQUFPLFNBQVMsQ0FBQTtJQUNqQixDQUFDO0NBeUNEIn0=