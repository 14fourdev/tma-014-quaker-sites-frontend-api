export const LOGIN = `
  mutation userLogin($input: NewLogin!) {
    login(input: $input) {
      token
    }
  }
`;
export const REGISTER = `
  mutation userRegister($input: NewConsumer!) {
    register(input: $input) {
      token
    }
  }
`;
export const REDEEM_CODE = `
  mutation codeRedeem($input: NewRedeemCode!) {
    redeemCode(input: $input) {
      id,
      instantWinner,
      prize,
      createdAt
    }
  }
`;
export const VALIDATE_CODES = `
  mutation validate($input: NewRedeemCode!){
    validate(input: $input) { 
      eligiblePrizes {
        id,
        name
      },
      valid
    }
  }
`;
export default {
    LOGIN,
    REGISTER,
    REDEEM_CODE,
    VALIDATE_CODES
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXV0YXRpb25zLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2xpYi9tdXRhdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsTUFBTSxDQUFDLE1BQU0sS0FBSyxHQUFHOzs7Ozs7Q0FNcEIsQ0FBQTtBQUVELE1BQU0sQ0FBQyxNQUFNLFFBQVEsR0FBRzs7Ozs7O0NBTXZCLENBQUE7QUFFRCxNQUFNLENBQUMsTUFBTSxXQUFXLEdBQUc7Ozs7Ozs7OztDQVMxQixDQUFBO0FBRUQsTUFBTSxDQUFDLE1BQU0sY0FBYyxHQUFHOzs7Ozs7Ozs7O0NBVTdCLENBQUE7QUFFRCxlQUFlO0lBQ2IsS0FBSztJQUNMLFFBQVE7SUFDUixXQUFXO0lBQ1gsY0FBYztDQUNmLENBQUEifQ==