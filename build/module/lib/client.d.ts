export declare class Client {
    private _clientId;
    private _apiBase;
    private _token;
    constructor(clientId: string, apiBase: string);
    setToken(token: string): void;
    decodeToken(token: string): object;
    checkExpired(token: string): boolean;
    request: (query: string, variables: any) => Promise<{
        response: Response;
        body: any;
    }>;
}
