export declare const LOGIN = "\n  mutation userLogin($input: NewLogin!) {\n    login(input: $input) {\n      token\n    }\n  }\n";
export declare const REGISTER = "\n  mutation userRegister($input: NewConsumer!) {\n    register(input: $input) {\n      token\n    }\n  }\n";
export declare const REDEEM_CODE = "\n  mutation codeRedeem($input: NewRedeemCode!) {\n    redeemCode(input: $input) {\n      id,\n      instantWinner,\n      prize,\n      createdAt\n    }\n  }\n";
export declare const VALIDATE_CODES = "\n  mutation validate($input: NewRedeemCode!){\n    validate(input: $input) { \n      eligiblePrizes {\n        id,\n        name\n      },\n      valid\n    }\n  }\n";
declare const _default: {
    LOGIN: string;
    REGISTER: string;
    REDEEM_CODE: string;
    VALIDATE_CODES: string;
};
export default _default;
