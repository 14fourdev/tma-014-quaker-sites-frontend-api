import Login from './interface/login';
import Redemption from './interface/redemption';
import User from './interface/user';
declare class Api {
    private _client;
    init(appId: string): void;
    validateToken(token: string): {
        valid: boolean;
    };
    setToken(token: string): void;
    logout(): void;
    login(login: Login): Promise<any>;
    register(user: User): Promise<any>;
    redeemCode(redemption: Redemption): Promise<any>;
    validate(redemption: Redemption): Promise<any>;
}
declare const _default: Api;
export default _default;
