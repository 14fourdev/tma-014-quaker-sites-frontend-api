export default interface Login {
    email: string;
    gRecaptchaResponse: string;
}
