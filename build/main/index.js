"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const debug_1 = __importDefault(require("debug"));
const client_1 = require("./lib/client");
const mutations_1 = require("./lib/mutations");
const log = debug_1.default('api:Api');
class Api {
    init(appId) {
        this._client = new client_1.Client(appId, '/graphql');
        log(`Quaker Sites API: ${appId}`);
    }
    validateToken(token) {
        if (this._client.checkExpired(token) === false) {
            return { valid: true };
        }
        else {
            return { valid: false };
        }
    }
    setToken(token) {
        if (this._client.checkExpired(token) === false) {
            this._client.setToken(token);
        }
        else {
            this.logout();
        }
    }
    logout() {
        this._client.setToken('');
    }
    login(login) {
        return this._client.request(mutations_1.LOGIN, { input: login });
    }
    register(user) {
        return this._client.request(mutations_1.REGISTER, { input: user });
    }
    redeemCode(redemption) {
        return this._client.request(mutations_1.REDEEM_CODE, { input: redemption });
    }
    validate(redemption) {
        return this._client.request(mutations_1.VALIDATE_CODES, { input: redemption });
    }
}
exports.default = new Api();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9zcmMvaW5kZXgudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxrREFBeUI7QUFDekIseUNBQXFDO0FBQ3JDLCtDQUE4RTtBQU05RSxNQUFNLEdBQUcsR0FBYSxlQUFLLENBQUMsU0FBUyxDQUFDLENBQUE7QUFFdEMsTUFBTSxHQUFHO0lBR0EsSUFBSSxDQUFDLEtBQWE7UUFDdkIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLGVBQU0sQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLENBQUE7UUFDNUMsR0FBRyxDQUFDLHFCQUFxQixLQUFLLEVBQUUsQ0FBQyxDQUFBO0lBQ25DLENBQUM7SUFFTSxhQUFhLENBQUMsS0FBYTtRQUNoQyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxLQUFLLEtBQUssRUFBRTtZQUM5QyxPQUFPLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFBO1NBQ3ZCO2FBQU07WUFDTCxPQUFPLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxDQUFBO1NBQ3hCO0lBQ0gsQ0FBQztJQUVNLFFBQVEsQ0FBQyxLQUFhO1FBQzNCLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLEtBQUssS0FBSyxFQUFFO1lBQzlDLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFBO1NBQzdCO2FBQU07WUFDTCxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUE7U0FDZDtJQUNILENBQUM7SUFFTSxNQUFNO1FBQ1gsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUE7SUFDM0IsQ0FBQztJQUVNLEtBQUssQ0FBQyxLQUFZO1FBQ3ZCLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsaUJBQUssRUFBRSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFBO0lBQ3RELENBQUM7SUFFTSxRQUFRLENBQUMsSUFBVTtRQUN4QixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLG9CQUFRLEVBQUUsRUFBRSxLQUFLLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQTtJQUN4RCxDQUFDO0lBRU0sVUFBVSxDQUFDLFVBQXNCO1FBQ3RDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsdUJBQVcsRUFBRSxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsQ0FBQyxDQUFBO0lBQ2pFLENBQUM7SUFFTSxRQUFRLENBQUMsVUFBc0I7UUFDcEMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQywwQkFBYyxFQUFFLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxDQUFDLENBQUE7SUFDcEUsQ0FBQztDQUNGO0FBRUQsa0JBQWUsSUFBSSxHQUFHLEVBQUUsQ0FBQSJ9