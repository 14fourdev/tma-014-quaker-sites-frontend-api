"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LOGIN = `
  mutation userLogin($input: NewLogin!) {
    login(input: $input) {
      token
    }
  }
`;
exports.REGISTER = `
  mutation userRegister($input: NewConsumer!) {
    register(input: $input) {
      token
    }
  }
`;
exports.REDEEM_CODE = `
  mutation codeRedeem($input: NewRedeemCode!) {
    redeemCode(input: $input) {
      id,
      instantWinner,
      prize,
      createdAt
    }
  }
`;
exports.VALIDATE_CODES = `
  mutation validate($input: NewRedeemCode!){
    validate(input: $input) { 
      eligiblePrizes {
        id,
        name
      },
      valid
    }
  }
`;
exports.default = {
    LOGIN: exports.LOGIN,
    REGISTER: exports.REGISTER,
    REDEEM_CODE: exports.REDEEM_CODE,
    VALIDATE_CODES: exports.VALIDATE_CODES
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXV0YXRpb25zLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2xpYi9tdXRhdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7QUFBYSxRQUFBLEtBQUssR0FBRzs7Ozs7O0NBTXBCLENBQUE7QUFFWSxRQUFBLFFBQVEsR0FBRzs7Ozs7O0NBTXZCLENBQUE7QUFFWSxRQUFBLFdBQVcsR0FBRzs7Ozs7Ozs7O0NBUzFCLENBQUE7QUFFWSxRQUFBLGNBQWMsR0FBRzs7Ozs7Ozs7OztDQVU3QixDQUFBO0FBRUQsa0JBQWU7SUFDYixLQUFLLEVBQUwsYUFBSztJQUNMLFFBQVEsRUFBUixnQkFBUTtJQUNSLFdBQVcsRUFBWCxtQkFBVztJQUNYLGNBQWMsRUFBZCxzQkFBYztDQUNmLENBQUEifQ==