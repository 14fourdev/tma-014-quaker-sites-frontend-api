"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jwt_decode_1 = __importDefault(require("jwt-decode"));
const debug_1 = __importDefault(require("debug"));
const log = debug_1.default('api:Client');
class Client {
    constructor(clientId, apiBase) {
        this.request = async (query, variables) => {
            const headers = new Headers();
            headers.append('Content-Type', 'application/json');
            headers.append('X-APP-ID', this._clientId);
            if (this._token !== '') {
                headers.append('Authorization', `Bearer ${this._token}`);
                //check for expired token
                //throw new Error(response.statusText)
            }
            const request = new Request(this._apiBase, {
                method: "POST",
                headers: headers,
                body: JSON.stringify({
                    query,
                    variables,
                }),
            });
            const response = await fetch(request);
            let body;
            try {
                body = await response.json();
            }
            catch (e) {
                log('Cant parse body', e);
            }
            if (!response.ok) {
                throw new Error(response.statusText);
            }
            return {
                response,
                body,
            };
        };
        this._token = '';
        this._clientId = clientId;
        this._apiBase = apiBase;
    }
    setToken(token) {
        this._token = token;
    }
    decodeToken(token) {
        if (token === '') {
            return {
                error: 'invalid-token'
            };
        }
        else {
            return jwt_decode_1.default(token);
        }
    }
    checkExpired(token) {
        let isExpired = true;
        const decoded = this.decodeToken(token);
        if (decoded.error) {
            log(decoded.error);
        }
        else {
            let now = new Date();
            let expires = new Date(decoded.exp * 1000);
            if (now > expires) {
                log('Expired Token');
            }
            else {
                isExpired = false;
            }
        }
        return isExpired;
    }
}
exports.Client = Client;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vLi4vc3JjL2xpYi9jbGllbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSw0REFBbUM7QUFDbkMsa0RBQXlCO0FBRXpCLE1BQU0sR0FBRyxHQUFZLGVBQUssQ0FBQyxZQUFZLENBQUMsQ0FBQTtBQUV4QyxNQUFhLE1BQU07SUFNZixZQUFhLFFBQWUsRUFBRSxPQUFlO1FBdUM3QyxZQUFPLEdBQUcsS0FBSyxFQUFFLEtBQWEsRUFBRSxTQUFjLEVBQUUsRUFBRTtZQUU5QyxNQUFNLE9BQU8sR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFBO1lBQzdCLE9BQU8sQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLGtCQUFrQixDQUFDLENBQUE7WUFDbEQsT0FBTyxDQUFDLE1BQU0sQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFBO1lBQzFDLElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxFQUFFLEVBQUU7Z0JBQ3BCLE9BQU8sQ0FBQyxNQUFNLENBQUMsZUFBZSxFQUFFLFVBQVUsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUE7Z0JBQ3hELHlCQUF5QjtnQkFDekIsc0NBQXNDO2FBQ3pDO1lBRUQsTUFBTSxPQUFPLEdBQUcsSUFBSSxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQXVCLEVBQUU7Z0JBQ3RELE1BQU0sRUFBRSxNQUFNO2dCQUNkLE9BQU8sRUFBRSxPQUFPO2dCQUNoQixJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQztvQkFDakIsS0FBSztvQkFDTCxTQUFTO2lCQUNaLENBQUM7YUFDTCxDQUFDLENBQUE7WUFFRixNQUFNLFFBQVEsR0FBRyxNQUFNLEtBQUssQ0FBRSxPQUFPLENBQUUsQ0FBQTtZQUN2QyxJQUFJLElBQUksQ0FBQTtZQUVSLElBQUk7Z0JBQ0EsSUFBSSxHQUFHLE1BQU0sUUFBUSxDQUFDLElBQUksRUFBRSxDQUFBO2FBQy9CO1lBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ1IsR0FBRyxDQUFDLGlCQUFpQixFQUFFLENBQUMsQ0FBQyxDQUFBO2FBQzVCO1lBRUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxFQUFFLEVBQUU7Z0JBQ2QsTUFBTSxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDeEM7WUFFRCxPQUFPO2dCQUNILFFBQVE7Z0JBQ1IsSUFBSTthQUNQLENBQUE7UUFDTCxDQUFDLENBQUE7UUEzRUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFLENBQUE7UUFDaEIsSUFBSSxDQUFDLFNBQVMsR0FBSSxRQUFRLENBQUE7UUFDMUIsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUE7SUFDM0IsQ0FBQztJQUVELFFBQVEsQ0FBRSxLQUFhO1FBQ25CLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFBO0lBQ3ZCLENBQUM7SUFFRCxXQUFXLENBQUUsS0FBYTtRQUN0QixJQUFHLEtBQUssS0FBSyxFQUFFLEVBQUU7WUFDYixPQUFPO2dCQUNILEtBQUssRUFBRSxlQUFlO2FBQ3pCLENBQUE7U0FDSjthQUFNO1lBQ0gsT0FBTyxvQkFBVSxDQUFDLEtBQUssQ0FBQyxDQUFBO1NBQzNCO0lBQ0wsQ0FBQztJQUVELFlBQVksQ0FBQyxLQUFhO1FBRXRCLElBQUksU0FBUyxHQUFZLElBQUksQ0FBQTtRQUNuQyxNQUFNLE9BQU8sR0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFBO1FBQzNDLElBQUcsT0FBTyxDQUFDLEtBQUssRUFBRTtZQUNqQixHQUFHLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFBO1NBQ2xCO2FBQU07WUFDTixJQUFJLEdBQUcsR0FBUyxJQUFJLElBQUksRUFBRSxDQUFBO1lBQzFCLElBQUksT0FBTyxHQUFTLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLENBQUE7WUFDaEQsSUFBRyxHQUFHLEdBQUcsT0FBTyxFQUFDO2dCQUNoQixHQUFHLENBQUMsZUFBZSxDQUFDLENBQUE7YUFDcEI7aUJBQU07Z0JBQ04sU0FBUyxHQUFHLEtBQUssQ0FBQTthQUNqQjtTQUNLO1FBRVAsT0FBTyxTQUFTLENBQUE7SUFDakIsQ0FBQztDQXlDRDtBQXBGRCx3QkFvRkMifQ==