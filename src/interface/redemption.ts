export default interface Redemption {
  codes: string[]
  retailer: string
}
