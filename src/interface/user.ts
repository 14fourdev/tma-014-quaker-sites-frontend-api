interface optin {
	label: string
}

export default interface User {
	email: string,
	firstName: string,
	lastName: string,
	zipcode: number,
	birthday: string,
	rules: boolean,
	optins: optin[],
	gRecaptchaResponse: string
}
