import debug from 'debug'
import { Client } from './lib/client'
import { LOGIN, REDEEM_CODE, REGISTER, VALIDATE_CODES } from './lib/mutations'

import Login from './interface/login'
import Redemption from './interface/redemption'
import User from './interface/user'

const log: Function = debug('api:Api')

class Api {
  private _client!: Client

  public init(appId: string) {
    this._client = new Client(appId, '/graphql')
    log(`Quaker Sites API: ${appId}`)
  }

  public validateToken(token: string) {
    if (this._client.checkExpired(token) === false) {
      return { valid: true }
    } else {
      return { valid: false }
    }
  }

  public setToken(token: string) {
    if (this._client.checkExpired(token) === false) {
      this._client.setToken(token)
    } else {
      this.logout()
    }
  }

  public logout() {
    this._client.setToken('')
  }

  public login(login: Login): Promise<any> {
    return this._client.request(LOGIN, { input: login })
  }

  public register(user: User): Promise<any> {
    return this._client.request(REGISTER, { input: user })
  }

  public redeemCode(redemption: Redemption): Promise<any> {
    return this._client.request(REDEEM_CODE, { input: redemption })
  }

  public validate(redemption: Redemption): Promise<any> {
    return this._client.request(VALIDATE_CODES, { input: redemption })
  }
}

export default new Api()
