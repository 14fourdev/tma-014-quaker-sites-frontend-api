import jwt_decode from 'jwt-decode'
import debug from 'debug'

const log:Function = debug('api:Client')

export class Client{
    
    private _clientId: string
    private _apiBase: string
    private _token: string

    constructor( clientId:string, apiBase: string) {
        this._token = ''
        this._clientId =  clientId
        this._apiBase = apiBase
    }

    setToken (token: string) {
        this._token = token
    }

    decodeToken (token: string):object {
        if(token === '') {
            return {
                error: 'invalid-token'
            }
        } else {
            return jwt_decode(token)
        }
    }

    checkExpired(token: string): boolean {

        let isExpired:boolean =  true
		const decoded:any = this.decodeToken(token)
		if(decoded.error) {
			log(decoded.error)
		} else {
			let now: Date = new Date()
			let expires: Date = new Date(decoded.exp * 1000)
			if(now > expires){
				log('Expired Token')
			} else {
				isExpired = false
			}
        }
        
		return isExpired
	}

    request = async (query: string, variables: any) => {

        const headers = new Headers()
        headers.append('Content-Type', 'application/json')
        headers.append('X-APP-ID', this._clientId)
        if (this._token !== '') {
            headers.append('Authorization', `Bearer ${this._token}`)
            //check for expired token
            //throw new Error(response.statusText)
        }

        const request = new Request(this._apiBase as RequestInfo, {
            method: "POST",
            headers: headers,
            body: JSON.stringify({
                query,
                variables,
            }),
        })

        const response = await fetch( request )
        let body
        
        try {
            body = await response.json()
        } catch (e) {
            log('Cant parse body', e)
        }
    
        if (!response.ok) {
            throw new Error(response.statusText);
        }

        return {
            response,
            body,
        }
    }

}