export const LOGIN = `
  mutation userLogin($input: NewLogin!) {
    login(input: $input) {
      token
    }
  }
`

export const REGISTER = `
  mutation userRegister($input: NewConsumer!) {
    register(input: $input) {
      token
    }
  }
`

export const REDEEM_CODE = `
  mutation codeRedeem($input: NewRedeemCode!) {
    redeemCode(input: $input) {
      id,
      instantWinner,
      prize,
      createdAt
    }
  }
`

export const VALIDATE_CODES = `
  mutation validate($input: NewRedeemCode!){
    validate(input: $input) { 
      eligiblePrizes {
        id,
        name
      },
      valid
    }
  }
`

export default {
  LOGIN,
  REGISTER,
  REDEEM_CODE,
  VALIDATE_CODES
}
